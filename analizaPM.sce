
// read the file as a matrix of strings
C_1 = csvRead('C:\Users\Admin\Desktop\MatLab\data\dane-pomiarowe_2019-10-08.csv', ';', ',', 'string');
C_2 = csvRead('C:\Users\Admin\Desktop\MatLab\data\dane-pomiarowe_2019-10-09.csv', ';', ',', 'string');
C_3 = csvRead('C:\Users\Admin\Desktop\MatLab\data\dane-pomiarowe_2019-10-10.csv', ';', ',', 'string');
C_4 = csvRead('C:\Users\Admin\Desktop\MatLab\data\dane-pomiarowe_2019-10-11.csv', ';', ',', 'string');
C_5 = csvRead('C:\Users\Admin\Desktop\MatLab\data\dane-pomiarowe_2019-10-12.csv', ';', ',', 'string');
C_6 = csvRead('C:\Users\Admin\Desktop\MatLab\data\dane-pomiarowe_2019-10-13.csv', ';', ',', 'string');
C_7 = csvRead('C:\Users\Admin\Desktop\MatLab\data\dane-pomiarowe_2019-10-14.csv', ';', ',', 'string');

// create cells for future data storing
D_1 = cell(24,2);
D_2 = cell(24,2);
D_3 = cell(24,2);
D_4 = cell(24,2);
D_5 = cell(24,2);
D_6 = cell(24,2);
D_7 = cell(24,2);

// a loop for saving just the important data (meaning the date and PM10 cancentration value)
for i=1:24
    D_1{i, 2} = C_1(i+1,6);
    D_2{i, 2} = C_2(i+1,6);
    D_3{i, 2} = C_3(i+1,6);
    D_4{i, 2} = C_4(i+1,6);
    D_5{i, 2} = C_5(i+1,6);
    D_6{i, 2} = C_6(i+1,6);
    D_7{i, 2} = C_7(i+1,6);
    D_1{i, 1} = ['2019-10-08 ',C_1(i+1,1)];
    D_2{i, 1} = ['2019-10-09 ',C_2(i+1,1)];
    D_3{i, 1} = ['2019-10-10 ',C_3(i+1,1)];
    D_4{i, 1} = ['2019-10-11 ',C_4(i+1,1)];
    D_5{i, 1} = ['2019-10-12 ',C_5(i+1,1)];
    D_6{i, 1} = ['2019-10-13 ',C_6(i+1,1)];
    D_7{i, 1} = ['2019-10-14 ',C_7(i+1,1)];
end

// find the highest PM10 concentration value (max_val) and it's corresponding date (date_cell)
max_val = 0
for i = 1:24
    if max_val < strtod(D_1{i,2})
        if max_val < strtod(D_2{i,2})
            if max_val < strtod(D_3{i,2})
                if max_val < strtod(D_4{i,2})
                    if max_val < strtod(D_5{i,2})
                        if max_val < strtod(D_6{i,2})
                            if max_val < strtod(D_7{i,2})
                                max_val = strtod(D_7{i,2});
                                date_cell = D_7{i,1};
                            end
                            max_val = strtod(D_6{i,2});
                            date_cell = D_6{i,1};
                        end
                        max_val = strtod(D_5{i,2});
                        date_cell = D_5{i,1};
                    end
                    max_val = strtod(D_4{i,2});
                    date_cell = D_4{i,1};
                end
                max_val = strtod(D_3{i,2});
                date_cell = D_3{i,1};
            end
            max_val = strtod(D_2{i,2});
            date_cell = D_2{i,1};
        end
        max_val = strtod(D_1{i,2});
        date_cell = D_1{i,1};
    end
end

// display the highest PM10 concentration value and it's corresponding date
mprintf('Highest value of PM10 concentration occured in %s at %s and equaled %d', date_cell(1,1), date_cell(1,2), max_val)

// create a 84x2 matrix of values measured at day and at night
for i = 1 : 24
    if i < 7 then
        DAY_NIGHT(i,2) = strtod(D_1{i, 2});
        DAY_NIGHT(i+12,2) = strtod(D_2{i, 2});
        DAY_NIGHT(i+24,2) = strtod(D_3{i, 2});
        DAY_NIGHT(i+36,2) = strtod(D_4{i, 2});
        DAY_NIGHT(i+48,2) = strtod(D_5{i, 2});
        DAY_NIGHT(i+60,2) = strtod(D_6{i, 2});
        DAY_NIGHT(i+72,2) = strtod(D_7{i, 2});
    elseif i >= 7 & i <= 18 then
        DAY_NIGHT(i-6,1) = strtod(D_1{i, 2});
        DAY_NIGHT(i+12-6,1) = strtod(D_2{i, 2});
        DAY_NIGHT(i+24-6,1) = strtod(D_3{i, 2});
        DAY_NIGHT(i+36-6,1) = strtod(D_4{i, 2});
        DAY_NIGHT(i+48-6,1) = strtod(D_5{i, 2});
        DAY_NIGHT(i+60-6,1) = strtod(D_6{i, 2});
        DAY_NIGHT(i+72-6,1) = strtod(D_7{i, 2});
    else
        DAY_NIGHT(i-12,2) = strtod(D_1{i, 2});
        DAY_NIGHT(i+12-12,2) = strtod(D_2{i, 2});
        DAY_NIGHT(i+24-12,2) = strtod(D_3{i, 2});
        DAY_NIGHT(i+36-12,2) = strtod(D_4{i, 2});
        DAY_NIGHT(i+48-12,2) = strtod(D_5{i, 2});
        DAY_NIGHT(i+60-12,2) = strtod(D_6{i, 2});
        DAY_NIGHT(i+72-12,2) = strtod(D_7{i, 2});
    end
end

// create variables for summing values
SUM_DAY = 0
SUM_NIGHT = 0

// sum daily and nightly values of PM10 concentration
for i = 1:84
    SUM_DAY = SUM_DAY + DAY_NIGHT(i, 1);
    SUM_NIGHT = SUM_NIGHT + DAY_NIGHT(i, 2);
end

// evaluate average values for day and night
AVG_DAY = SUM_DAY / 84;
AVG_NIGHT = SUM_NIGHT / 84;

// create variables for summing values (later used for standard deviastion evaluation)
DEV_SUM_DAY = 0;
DEV_SUM_NIGHT = 0;

// sume the values
for i = 1:84
    DEV_SUM_DAY = DEV_SUM_DAY + (DAY_NIGHT(i, 1) - AVG_DAY)^2;
    DEV_SUM_NIGHT = DEV_SUM_NIGHT + (DAY_NIGHT(i, 2) - AVG_NIGHT)^2;
end

// evaluate standart deviation of day and night averages
for i = 1:84
    DEV_DAY = sqrt(DEV_SUM_DAY / 84) / sqrt(84);
    DEV_NIGHT = sqrt(DEV_SUM_NIGHT / 84) / sqrt(84);
end

// evaluate statistical indicator informing about the difference in daytime and nighttime PM10 concetration 
STAT = abs(AVG_DAY - AVG_NIGHT) / sqrt(DEV_DAY^2 + DEV_NIGHT^2);

// set a file for saving data
fd = mopen('C:\Users\Admin\Desktop\MatLab\PMresult.dat.txt', 'wt');
// and save part of the data
mfprintf(fd,'Highest value of PM10 concentration occured in %s at %s and equaled %d \n', date_cell(1,1), date_cell(1,2), max_val);

// check for the statistical difference and save conclusion in file
if STAT < 2 then
    disp('There is no statistical difference in daytime and nighttime PM10 concetration');
    mfprintf(fd, 'There is no statistical difference in daytime and nighttime PM10 concetration');
elseif STAT > 3 then
    disp('There is statistical difference in daytime and nighttime PM10 concentration');
    mfprintf(fd, 'There is statistical difference in daytime and nighttime PM10 concentration');
else
    disp('It cannot be determined if there is a statisticly significant difference betweene the PM10 concentration during daytime and nighttime');
    mfprintf(fd, 'It cannot be determined if there is a statisticly significant difference betweene the PM10 concentration during daytime and nighttime');
end

// close the file
mclose(fd);

